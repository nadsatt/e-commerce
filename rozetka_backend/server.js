const express = require('express');
const bodyParser = require("body-parser");
const {products, reviews, categories, users} = require('./db');
const port = 3000;

const server = express();
server.use(bodyParser.json());

server.listen(port, ()=>{
    console.log('Server listening at http://localhost:', port);
})


// GET
server.get('/api/categories', (req, res) => {
    res.send(categories);
});

server.get('/api/reviews', (req, res) => {
    res.send(reviews);
});

server.get('/api/products', (req, res) => {
    res.send(products);
});


// POST
server.post('/api/reviews', (req, res) => {
console.log(req.body)
    let id = reviews[reviews.length - 1].id + 1;
    let review = {...req.body, id};
    console.log(review);
    reviews.push(review);

    res.status(200).send(review);
});

server.post('/api/authenticate', (req, res) => {
    let {username, password} = req.body;

    let user = users.find(user => user.username === username && user.password === password);

    if(!user) res.status(401).send('Invalid username or password.');
    else res.send(getUserWithToken(user));
});

server.post('/api/register', (req, res) => {
    let {username, password, firstName, lastName} = req.body;

    let id = users[users.length - 1].id + 1;
    let newUser = {id, username, password, firstName, lastName};
    users.push(newUser);

    res.send(getUserWithToken(newUser));
})


// FUNCTIONS
function getUserWithToken(user){
    user = {...user, token: 'fake-jwt-token'};
    delete user.password;
    return user;
}



