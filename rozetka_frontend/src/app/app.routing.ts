import { RouterModule, Routes } from '@angular/router'
import { HomePageComponent } from './rozetka/home-page/home-page.component';
import { LoginPageComponent } from './rozetka/login-page/login-page.component';
import { ProductPageComponent } from './rozetka/product-page/product-page.component';
import { RegisterPageComponent } from './rozetka/register-page/register-page.component';
import { UserPageComponent } from './rozetka/user-page/user-page.component';
import { AuthGuard } from './shared/_helpers/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: HomePageComponent
  },
  {
    path:'cabinet',
    component: UserPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path: 'register',
    component: RegisterPageComponent
  },
  {
    path: ':id',
    component: ProductPageComponent,
  }
]

export const appRoutingModule = RouterModule.forRoot(routes);
