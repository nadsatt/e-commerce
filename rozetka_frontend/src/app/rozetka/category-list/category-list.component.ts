import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/shared/_models/category';
import { CategoryService } from 'src/app/shared/_services/category.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})

export class CategoryListComponent implements OnInit {

  constructor(private categoryService: CategoryService) { }

  categories: Category[];

  ngOnInit(): void {
    this.getCategories();
  }

  getCategories(): void {
    this.categoryService.getCategories().subscribe(
      categories => this.categories = categories,
      err => console.error(err)
    );
  }

  selectCategory(category: string): void {
    this.categoryService.selectedCategorySubject.next(category);
  }
}
