import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/shared/_services/auth.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})

export class LoginFormComponent implements OnInit {

  @Input() returnUrl: string;

  loginForm: FormGroup;
  error: string;
  success: string;

  get username(): AbstractControl {
    return this.loginForm.controls.username;
  }
  get password(): AbstractControl {
    return this.loginForm.controls.password;
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private fb: FormBuilder) {
      if (this.authService.currentUserValue) this.router.navigate(['/']);
    }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login(): void {
    this.authService.login(this.username.value, this.password.value)
      .subscribe(
        user => {
          this.success = 'You logged in!';
          setTimeout(() => this.router.navigate([this.returnUrl]), 1000);
        },
        err => {
          this.loginForm.reset();
          this.error = err.error;
          setTimeout(() => { this.error = ''}, 700)
        }
      );
  }
}
