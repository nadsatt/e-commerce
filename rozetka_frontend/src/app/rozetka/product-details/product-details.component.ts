import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/shared/_models/product';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})

export class ProductDetailsComponent implements OnInit {

  @Input() product: Product;
  selectedImgUrl: string;

  constructor(){}

  ngOnInit(): void {}

  changeProductImage(ul: HTMLUListElement,
    selectedLi: HTMLImageElement,
    currentImg: HTMLImageElement): void {
      
    let lis = ul.querySelectorAll('li');
    lis.forEach(li => li.classList.remove('all-images__item--active'));
    selectedLi.classList.add('all-images__item--active');

    this.selectedImgUrl= selectedLi.firstElementChild.getAttribute('src');
    currentImg.setAttribute('src', this.selectedImgUrl);
  }
}
