import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/shared/_models/product';
import { CategoryService } from 'src/app/shared/_services/category.service';
import { ProductService } from 'src/app/shared/_services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})

export class ProductListComponent implements OnInit {

  constructor(private productService: ProductService,
    private categoryService: CategoryService) { }

  products: Product[];

  ngOnInit(): void {
    this.getProductsByCategory(this.categoryService.selectedCategorySubject.value);

    this.categoryService.selectedCategoryObservable.subscribe(
      category => this.getProductsByCategory(category)
    );
  }

  getProductsByCategory(category: string): void {
    this.productService.getProductsByCategory(category).subscribe(
      products => this.products = products
    )
  }
}
