import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/shared/_models/product';
import { ProductService } from 'src/app/shared/_services/product.service';

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.scss']
})

export class ProductPageComponent implements OnInit {

  constructor(private route:ActivatedRoute,
              private productService: ProductService) { }

  product: Product;

  ngOnInit(): void {
    this.route.params
      .subscribe(
        params => this.getProductById(params.id)
      )
  }

  getProductById(id): void {
    this.productService.getProductById(+id)
      .subscribe(
        product => this.product = product
      );
  }
}
