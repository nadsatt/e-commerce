import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Review } from 'src/app/shared/_models/review';
import { User } from 'src/app/shared/_models/user';
import { AuthService } from 'src/app/shared/_services/auth.service';
import { ReviewService } from 'src/app/shared/_services/review.service';

@Component({
  selector: 'app-product-review-form',
  templateUrl: './product-review-form.component.html',
  styleUrls: ['./product-review-form.component.scss']
})

export class ProductReviewFormComponent implements OnInit {

  @Input() productId: number;

  get currentUserFullName(): string {
    let user = this.authService.currentUserValue;
    if (!user) return;
    return user.firstName + ' ' + user.lastName;
  }

  get currentUser(): User {
    return this.authService.currentUserValue;
  }

  constructor(
    private reviewService: ReviewService,
    private authService: AuthService,
    private router: Router) { }

  ngOnInit(): void {}

  addReview(inputText: HTMLInputElement): void {
    let review: Review = {
      productId: this.productId,
      userFirstName: this.currentUser.firstName,
      userLastName: this.currentUser.lastName,
      userId: this.currentUser.id,
      text: inputText.textContent,
      id:null
    };

    this.reviewService.addReviewByProduct(review)
      .subscribe({
        next: review => {
          this.reviewService.reviewPosted.next(true);
          inputText.textContent = '';
        },
        error: err => console.log(err)
      });
  }

  checkIfUserLoggedIn(): void {
    if(!this.authService.currentUserValue){
      this.router.navigate(['/login']);
    }
  }
}
