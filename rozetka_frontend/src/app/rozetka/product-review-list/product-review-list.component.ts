import { Component, Input, OnInit } from '@angular/core';
import { Review } from 'src/app/shared/_models/review';
import { ReviewService } from 'src/app/shared/_services/review.service';

@Component({
  selector: 'app-product-review-list',
  templateUrl: './product-review-list.component.html',
  styleUrls: ['./product-review-list.component.scss']
})

export class ProductReviewListComponent implements OnInit {

  constructor(private reviewService: ReviewService) { }

  @Input() productId: number;
  reviews: Review[];

  ngOnInit(): void {
    this.getProductReviews(+this.productId);

    this.reviewService.reviewPosted$
      .subscribe(
        () => this.getProductReviews(this.productId)
      );
  }

  private getProductReviews(productId: number): void {
    this.reviewService.getReviewsByProduct(productId)
      .subscribe(
        reviews => this.reviews = reviews
      );
  }
}
