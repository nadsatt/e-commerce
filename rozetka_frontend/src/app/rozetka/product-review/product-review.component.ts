import { Component, Input, OnInit } from '@angular/core';
import { Review } from 'src/app/shared/_models/review';

@Component({
  selector: 'app-product-review',
  templateUrl: './product-review.component.html',
  styleUrls: ['./product-review.component.scss']
})
export class ProductReviewComponent implements OnInit {

  @Input() review: Review;

  constructor() { }

  ngOnInit(): void {
  }
}
