import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/_services/auth.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {

  registerForm: FormGroup;
  error: string;
  success: string;
  pattern: string = '^(?=.*[0-9]{2})(?=.*[a-z]{2})(?=.*[A-Z]{2})(?=.*[!@#$%^&*_]{2}).{8,}$';

  get username(): AbstractControl {
    return this.registerForm.controls.username;
  }
  get password(): AbstractControl {
    return this.registerForm.controls.password;
  }
  get firstName(): AbstractControl {
    return this.registerForm.controls.firstName;
  }
  get lastName(): AbstractControl {
    return this.registerForm.controls.lastName;
  }

  constructor(
    private router: Router,
    private authService: AuthService,
    private fb: FormBuilder
  ) {
    if (this.authService.currentUserValue) this.router.navigate(['/']);
  }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(this.pattern)]],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required]
    });
  }

  register(): void {
    this.authService.register(this.username.value, this.password.value, this.firstName.value, this.lastName.value)
      .subscribe(
        user => {
          this.success = 'You registered!';
          setTimeout(() => this.router.navigate(['/']), 1000);
        },
        err => this.error = err.error
      )
  }
}
