import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryListComponent } from './category-list/category-list.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductComponent } from './product/product.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { HomePageComponent } from './home-page/home-page.component';
import { RouterModule } from '@angular/router';
import { ProductPageComponent } from './product-page/product-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginPageComponent } from './login-page/login-page.component';
import { UserPageComponent } from './user-page/user-page.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import { RegisterFormComponent } from './register-form/register-form.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { ProductReviewFormComponent } from './product-review-form/product-review-form.component';
import { ProductReviewListComponent } from './product-review-list/product-review-list.component';
import { ProductReviewComponent } from './product-review/product-review.component';

@NgModule({
  declarations: [
    HomePageComponent,
    CategoryListComponent,
    ProductListComponent,
    ProductComponent,
    ProductDetailsComponent,
    ProductPageComponent,
    LoginPageComponent,
    UserPageComponent,
    RegisterPageComponent,
    RegisterFormComponent,
    LoginFormComponent,
    ProductReviewFormComponent,
    ProductReviewListComponent,
    ProductReviewComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    HomePageComponent,
    CategoryListComponent,
    ProductListComponent,
    ProductPageComponent,
    ProductComponent,
    ProductDetailsComponent,
    ProductReviewComponent,
    ProductReviewListComponent,
    ProductReviewFormComponent,
    LoginPageComponent,
    LoginFormComponent,
    RegisterPageComponent,
    RegisterFormComponent
  ]
})

export class RozetkaModule { }
