import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/shared/_models/user';
import { AuthService } from 'src/app/shared/_services/auth.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})

export class UserPageComponent implements OnInit {

  user: User;

  constructor(
    private authService: AuthService,
    private router: Router) { }

  ngOnInit(): void {
    this.subscribeToUserValue();
    this.user = this.authService.currentUserValue;
  }

  subscribeToUserValue(): void {
    this.authService.currentUserObservable.subscribe({
      next: user => {
        this.user = user;
        this.navigateAwayIfUserNonAuthenticated();
      },
      error: err => console.error('Error while trying to get current user in UserPageComponent: ', err)
    });
  }

  navigateAwayIfUserNonAuthenticated(): void {
    if (!this.user) {
      this.router.navigate(['/login']);
    }
  }
}
