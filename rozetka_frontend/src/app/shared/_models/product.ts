import { Category } from './category';

export class Product {
  id: number;
  name: string;
  urls: string[];
  description: string;
  price: string;
  category: Category;
}
