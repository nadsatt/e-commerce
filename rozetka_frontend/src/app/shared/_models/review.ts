export interface Review {
  id: number;
  productId: number;
  userId: number;
  userFirstName: string;
  userLastName: string;
  text: string;
}
