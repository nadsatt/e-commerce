import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { User } from '../_models/user';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor(private http: HttpClient){}

  private currentUserSubject = new BehaviorSubject(JSON.parse(sessionStorage.getItem('currentUser')));
  currentUserObservable = this.currentUserSubject.asObservable();

  get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string): Observable<User> {
    return this.http.post<User>('api/authenticate', {username, password})
      .pipe(map(user => {
        sessionStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        return user;
      }))
      .pipe(catchError(err => throwError(err)));
  }

  logout(): void {
    sessionStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

  register(username: string, password: string, firstName: string, lastName: string): Observable<User> {
    return this.http.post<User>('api/register', {username, password, firstName, lastName, id: null})
    .pipe(map(user => {
      sessionStorage.setItem('currentUser', JSON.stringify(user));
      this.currentUserSubject.next(user);
      return user;
    }))
    .pipe(catchError(err => throwError(err)));
  }
}
