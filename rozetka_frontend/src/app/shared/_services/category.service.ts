import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Category } from '../_models/category';

@Injectable({
  providedIn: 'root'
})

export class CategoryService {

  constructor(private http: HttpClient) {  }

  private rootRoute = '/api';

  selectedCategorySubject = new BehaviorSubject('Ноутбуки и компьютеры');
  selectedCategoryObservable = this.selectedCategorySubject.asObservable();

  getCategories(): Observable<Category[]> {
    return this.http.get<string[]>(this.rootRoute + '/categories')
    .pipe(
      map(categories => categories.map(
        category => new Category(null, category, null))
    ));
  }
}
