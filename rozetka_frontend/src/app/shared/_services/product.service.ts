import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Category } from '../_models/category';
import { Product } from '../_models/product';

@Injectable({
  providedIn: 'root'
})

export class ProductService {
  constructor(private http: HttpClient) { }

  private rootRoute = '/api';

  getProductsByCategory(category: string): Observable<Product[]> {
    return this.http.get<any[]>(this.rootRoute + '/products')
    .pipe(
      map(products => products.map(product => ({...product, category: new Category(0, product.category, null)}))),
      map(products => products.filter(product => product.category.name === category))
    );
  }

  getProductById(id: number): Observable<Product> {
    return this.http.get<Product[]>(this.rootRoute + '/products').pipe(
      map(products => products.find(product => product.id === id))
    );
  }
}
