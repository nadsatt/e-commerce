import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Review } from '../_models/review';

@Injectable({
  providedIn: 'root'
})

export class ReviewService {

  reviewPosted = new Subject();
  reviewPosted$ = this.reviewPosted.asObservable();

  constructor(private http: HttpClient) { }

  private rootRoute = '/api';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
  })};

  getReviewsByProduct(productId: number): Observable<Review[]> {
    return this.http.get<Review[]>(this.rootRoute + '/reviews')
      .pipe(
        map(reviews => reviews.filter(review => review.productId === productId)),
        map(reviews => reviews.reverse())
      );
  }

  addReviewByProduct(review: Review): Observable<Review> {
    return this.http.post<Review>(this.rootRoute + '/reviews', review, this.httpOptions);
  }
}

